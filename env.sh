# Consistent LCG environment (http://lcginfo.cern.ch)
. /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_95 x86_64-slc6-gcc8-opt

export HZZ2L2NU_BASE=$(pwd)
export PATH=$HZZ2L2NU_BASE/bin:$PATH

